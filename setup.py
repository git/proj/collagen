#!/usr/bin/env python

from distutils.core import setup
import os

VERSION='1.0'

webfiles = []
for dirpath, dirnames, filenames in os.walk('web/'):
    dirprefix = "/usr/share/collagen-%s/%s" % (VERSION, dirpath)
    for fname in filenames:
        webfiles.append((dirprefix, [os.path.join(dirpath, fname)]))

for dirpath, dirnames, filenames in os.walk('examples/'):
    dirprefix = "/usr/share/doc/collagen-%s" % VERSION
    for fname in filenames:
        webfiles.append((dirprefix, [os.path.join(dirpath, fname)]))

dirprefix = "/usr/share/collagen-%s" % VERSION
webfiles.append((dirprefix, ['initial_data.json']))

setup(name='Collagen',
      version=VERSION,
      description='Gentoo portage tree-wide files database',
      author='Stanislav Ochotnicky',
      author_email='stanislav@ochotnicky.com',
      url='http://soc.gentooexperimental.org/projects/show/collision-database',
      package_dir = {'': 'src'},
      packages=['collagen/',
          'collagen/tinderbox',
          'collagen/matchbox',
          'collagen/matchbox/db',
          'collagen/matchbox/db/main',
          'collagen/common',
          'collagen/protocol',
          'collagen/util/'],
      scripts=['src/matchbox.py', 'src/tinderbox.py','util/mktinderboxchroot.sh'],
      data_files = webfiles
     )

