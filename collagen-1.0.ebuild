# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit distutils git

DESCRIPTION="Gentoo tree-wide file database and QA tool"
HOMEPAGE="http://soc.gentooexperimental.org/projects/show/collision-database"
EGIT_REPO_URI="git://git.overlays.gentoo.org/proj/collagen.git"
EGIT_TREE="release-1.0"

IUSE=""
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"

DEPEND="virtual/python
		dev-python/django"


