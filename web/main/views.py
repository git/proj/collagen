# Create your views here.

from django.shortcuts import render_to_response, get_object_or_404
from django.http import Http404,HttpResponse
from django.utils.html import escape
from django.core.exceptions import ObjectDoesNotExist


from web.main.models import *

def index(request):
    last_pkgs = Package.objects.all().order_by('-id')[:5]
    cats = PackageCategory.objects.all()
    print cats
    return render_to_response('index.html', {'latest_packages': last_pkgs,'categories':cats})

def browse(request, category_name=None, pkg_name=None):
    cats = PackageCategory.objects.all()
    only_failed = False
    if request.method == "GET" and "failed" in request.GET:
        only_failed=True
    if not category_name and not pkg_name:
        if not only_failed:
            cats = PackageCategory.objects.order_by('name')
        else:
            pprops = PackageProperties.objects.filter(error_code__gt=0)
            cats = set()
            for pprop in pprops:
                cats.add(pprop.packageversion.category)
            cats = sorted(cats)
        return render_to_response('browse-categories.html', {'objects':cats,'failed': only_failed})
    cat = PackageCategory.objects.get(name=category_name)
    if category_name and not pkg_name:
        pversions = cat.packageversion_set.all()
        packages = set()
        for pv in pversions:
            pprops = pv.packageproperties_set.filter(error_code__gt=0)
            if len(pprops) == 0 and only_failed:
                continue
            packages.add(pv.package.name)
        packages = sorted(packages)
        return render_to_response('browse-categories.html',{'objects':packages,'category':cat, 'failed': only_failed})

    pkg = Package.objects.get(name=pkg_name)
    pversions = PackageVersion.objects.filter(package=pkg, category=cat)
    if only_failed:
        pv_subset = []
        for pv in pversions:
            if 0 < len(pv.packageproperties_set.filter(error_code__gt=0)):
                pv_subset.append(pv)
        pversions = pv_subset

    return render_to_response('view-package.html',{'pversions':pversions})


def view(request, category_name, pkg_name, pkg_ver):
    cat = PackageCategory.objects.get(name=category_name)
    pkg = Package.objects.get(name=pkg_name)
    pv = PackageVersion.objects.filter(package=pkg, category = cat, version = pkg_ver)
    if not pv or len(pv) == 0:
        raise Http404

    pv = pv[0]
    pprops = pv.packageproperties_set.order_by('-timeadded')
    cpv = "%s/%s-%s" % (category_name, pkg_name, pkg_ver)
    return render_to_response('view-package-props.html',{'pprops':pprops,'cpv':cpv})

def view_attachment(request, attachment_id):
    try:
        at = Attachment.objects.get(pk=attachment_id)
        return HttpResponse(at.content, content_type="%s; charset=utf-8" % at.mimetype)
    except Attachment.DoesNotExist:
        raise Http404


def view_content(request, packageproperties_id):
    try:
        pp = PackageProperties.objects.get(pk=packageproperties_id)
        ppfs = pp.packageproperties_file_set.order_by('file')
        return render_to_response('view-content.html', {'ppfs':ppfs})
    except PackageProperties.DoesNotExist:
        raise Http404


def search_by_path(request, path):
    if request.method == "GET" and "path" in request.GET:
        path = request.GET["path"]
    try:
        file = File.objects.get(path=path)
        ppfs = file.packageproperties_file_set.all()
        cpvs = set()
        for ppf in ppfs:
            cpvs.add("%s/%s-%s" % (ppf.packageproperties.packageversion.category.name,
                                   ppf.packageproperties.packageversion.package.name,
                                   ppf.packageproperties.packageversion.version))
        cpvs = sorted(cpvs)
    except File.DoesNotExist:
        return render_to_response('404.html', {'path':path})
    return render_to_response('list-packages-with-file.html',{'cpvs': cpvs, 'path':path})
