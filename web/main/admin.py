from web.main.models import *
from django.contrib import admin

admin.site.register(Package)
admin.site.register(PackageVersion)
admin.site.register(PackageCategory)
admin.site.register(PortageProfile)
admin.site.register(Tinderbox)
admin.site.register(PackageProperties)
admin.site.register(Attachment)
admin.site.register(Useflag)
admin.site.register(File)
admin.site.register(FileType)
admin.site.register(PackageProperties_File)
