from django.conf.urls.defaults import *
from django.conf import settings

prefix = 'web.main.views.'

urlpatterns = patterns('',
    (r'^$', prefix+'index'),
    (r'^browse$', prefix+'browse'),
    (r'^browse/(?P<category_name>\w+-\w+)$', prefix+'browse'),
    (r'^browse/(?P<category_name>\w+(-\w+){0,1})/(?P<pkg_name>.*)$', prefix+'browse'),
    (r'^view/package/(?P<category_name>\w+(-\w*){0,1})/(?P<pkg_name>[a-zA-Z_-]+)-(?P<pkg_ver>[0-9_.r-]+)$', prefix+'view'),
    (r'^view/attachment/(?P<attachment_id>\d+)/.*$', prefix+'view_attachment'),
    (r'^view/content/(?P<packageproperties_id>\d+)$', prefix+'view_content'),
    (r'^search/by-path/(?P<path>.*)$', prefix+'search_by_path'),
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_DIR, 'show_indexes': True}),
)
