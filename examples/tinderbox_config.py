MATCHBOX_HOST="localhost"
MATCHBOX_PORT=10000

MK_CHROOT_SCRIPT="/usr/bin/mktinderboxchroot.sh"
STAGE_TARBALL="/data/downloads/stage3-i686-2008.0.tar.bz2"

WORKDIR="/collagen"

BASE_CHROOT=WORKDIR+"/collagen-base-chroot"
WORK_CHROOT=WORKDIR+"/collagen-work-chroot"

CHROOT_LOGS="/logs"

import logging as log
LOG_LEVEL=log.DEBUG


