import os
from errno import *
import logging as log
import tinderbox_config as config

fhandler = None

def init_logging(dir):
    """Initialize logging file and settings
    """
    global fhandler
    try:
        os.mkdir(dir)
    except OSError, e:
        if e.errno != EEXIST:
            raise e

    if fhandler is not None:
        fhandler.close()
        log.getLogger('').removeHandler(fhandler)
    fhandler = log.FileHandler(dir+"/tinderbox.log")
    formatter = log.Formatter('%(asctime)s %(levelname)s %(message)s')
    fhandler.setFormatter(formatter)
    log.getLogger('').addHandler(fhandler)
    fhandler.setLevel(config.LOG_LEVEL)
