import sys
sys.path.append("../..")
from tinderbox import Tinderbox

import unittest
import portage
from util import flatten_deps

class TinderboxTest(unittest.TestCase):

    def setUp(self):
        self.tb = Tinderbox()
        self.trees = portage.create_trees()
        self.portapi = self.trees[portage.root]["porttree"].dbapi
        self.portset = portage.config(clone=portage.settings)
        self.pkgs = ['dev-db/hsqldb-1.8.0.10','app-shells/bash-3.2_p39','sys-apps/portage-2.1.4.5']

    def testCreateDepGroups(self):
        settings = self.portset
        portapi = self.portapi
        for pkg in self.pkgs:
            deps = portapi.aux_get(pkg, ["DEPEND"])
            deps = portage.dep.paren_reduce(deps[0])
            settings.setcpv(pkg, mydb=self.portapi)
            use_enabled = set(settings["PORTAGE_USE"].split())
            iuse = set(settings["IUSE"].split())
            use_deps = portage.dep.use_reduce(deps, list(use_enabled & iuse))

            use_deps = self.tb._normalize_dependencies(use_deps)
            use_deps = flatten_deps(use_deps)
            self.tb.create_dep_groups(use_deps)

    def testNormalizeDeps(self):
        settings = self.portset
        portapi = self.portapi
        for pkg in self.pkgs:
            deps = portapi.aux_get(pkg, ["DEPEND"])
            deps = portage.dep.paren_reduce(deps[0])
            settings.setcpv(pkg, mydb=self.portapi)
            use_enabled = set(settings["PORTAGE_USE"].split())
            iuse = set(settings["IUSE"].split())
            use_deps = portage.dep.use_reduce(deps, list(use_enabled & iuse))
            use_deps = self.tb._normalize_dependencies(use_deps)
            for dep in use_deps:
                self.assertTrue(portage.dep.isvalidatom(dep))




if __name__ == '__main__':
    unittest.main()
