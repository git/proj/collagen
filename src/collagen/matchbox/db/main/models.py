from django.db import models

# Create your models here.
class Package(models.Model):
    name = models.CharField(max_length=255,unique=True)

class PackageVersion(models.Model):
    version = models.CharField(max_length=255)
    package = models.ForeignKey('Package')
    category = models.ForeignKey('PackageCategory')
    dependencies = models.ManyToManyField('self', symmetrical=False)

class PackageCategory(models.Model):
    name = models.CharField(max_length=255,unique=True)

class PortageProfile(models.Model):
    name = models.CharField(max_length=255,unique=True)

class Tinderbox(models.Model):
    ip = models.IPAddressField()

class PackageProperties(models.Model):
    packageversion = models.ForeignKey('PackageVersion')
    profile = models.ForeignKey('PortageProfile')
    tinderbox = models.ForeignKey('Tinderbox')
    error_code = models.IntegerField()
    contents = models.ManyToManyField('File',through='PackageProperties_File')
    useflags = models.ManyToManyField('Useflag')
    timeadded = models.DateTimeField(auto_now_add=True)

class Attachment(models.Model):
    packageproperties = models.ForeignKey('PackageProperties')
    name = models.CharField(max_length=255)
    mimetype = models.CharField(max_length=255)
    content = models.TextField()

class Useflag(models.Model):
    name = models.CharField(max_length=255, unique=True)

class File(models.Model):
    path = models.CharField(max_length=65535, unique=True)
    class Meta:
        ordering = ('path',)

class FileType(models.Model):
    name = models.CharField(max_length=20, unique=True)

class PackageProperties_File(models.Model):
    file = models.ForeignKey('File')
    filetype = models.ForeignKey('FileType')
    packageproperties = models.ForeignKey('PackageProperties')
    hash = models.CharField(max_length=32,blank=True)
    size = models.IntegerField(blank=True)
