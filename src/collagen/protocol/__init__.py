

class MatchboxCommand(object):
    """
    Base class for Matchbox network commands
    """
    pass

class GetNextPackage(MatchboxCommand):
    """
    Matchbox command that will make matchbox return with GetNextPackageReply
    """
    pass

class PackageInfo(object):
    """
    Package info that will be inserted into database
    """
    def __init__(self):
        """
        name: package name (CP) e.g. dev-util/git
        version: version including revision 1.6.3.1-r1
        content: dict of contents as returned by dblink.getcontents()
        use_flags: use flags enabled for compilation of this package
        attachments: dict holding additional information (build logs, environment, etc)
        error: None if no error, else holds error code
        emerge_info: output from emerge --info
        depends: list of dependencies this package has been built with in CPV form
        profile: portage profile enabled (e.g. default/linux/x86/2008.0/desktop)
        """
        self.name = None
        self.version = None
        self.content = {}
        self.use_flags = []
        self.attachments = None
        self.error = None
        self.emerge_info = None
        self.depends = []
        self.profile = None
    def __str__(self):
        ret ="Name: %s\n" % self.name + \
             "Version: %s\n" % self.version + \
             "Content lenght: %d\n" % len(self.content) + \
             "Content paths: %s\n" % self.content.keys() + \
             "Emerge info: %s\n" % self.emerge_info + \
             "Use flags: %s\n" % self.use_flags + \
             "Profile: %s\n" % self.profile + \
             "Depends: %s\n" % str(self.depends)
        for key in self.attachments.keys():
            ret = ret + "Attachment %s: \n%s\n" % (key, self.attachments[key])
        return ret

class AddPackageInfo(MatchboxCommand):
    def __init__(self, package_infos):
        """
	Command for Matchbox to add information about packages into database

        @param package_infos: List of PackageInfo objects
	@type package_infos: list
        """
        if type(package_infos) is not list:
            raise TypeError("Parameter need to be a list of PackageInfo objects")
        self.package_infos = package_infos



class MatchboxReply(object):
    """
    Base class for Matchbox network replies
    """
    pass

class GetNextPackageReply(MatchboxReply):
    """
    Reply for GetNextPackage command. This specifies package that should be compiled
    by tinderbox
    """

    def __init__(self, package_name, version=None, use_flags=None):
        """
        @param package_name: package name and category (CP). e.g dev-util/git
        @type package_name: string
        @param version: version of package to compile. If None all versions will be compiled
        @type version: string
        @param use_flags: extra use flags to enable/disable for this build
        @type use_flags: list of strings (e.g. ['-unicode','gtk'])

        """
        self.package_name = package_name
        self.version = version
        self.use_flags = use_flags
