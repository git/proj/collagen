

class WritableObject:
    """Class for buffering output

    used in various places for catching stdout/stderr of emerge 
    library functions
    """
    def __init__(self):
        self.content = []
    def write(self, string):
        self.content.append(string)
    def isatty(self):
        return 0
    def flush(self):
        pass
    def getvalue(self):
        ret = ""
        for part in self.content:
            ret = ret + part
        return ret

def flatten_deps(deps):
    """
    Takes nested deps returned from portage.dep.use_reduce and flattens them to simple list

    @param deps: list of dependencies returned from portage.dep.use_reduce()
    @type deps: List
    @rtype: List
    @return: Flattened list of deps
    """
    result = []
    for el in deps:
        if hasattr(el, "__iter__") and not isinstance(el, basestring):
            result.extend(flatten_deps(el))
        else:
            result.append(el)
    return result 
