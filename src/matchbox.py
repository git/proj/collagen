#!/usr/bin/python

import sys
import os
sys.path.insert(0, os.getcwd())

from collagen.matchbox import MatchboxServer as ms

s = ms('localhost',10000)

s.start_server()
