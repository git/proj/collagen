#!/usr/bin/python

import sys
import os
sys.path.insert(0, os.getcwd())

from collagen.tinderbox import Tinderbox, Package

tb = Tinderbox()
tb.start_tinderbox()
